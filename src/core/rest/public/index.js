import Axios from 'axios'

const http = Axios.create({
  baseURL: process.env.API_BASE,
  timeout: 20000
})

export default {
  signUp: payload => http.post('/account', payload),
  signIn: payload => http.put('/account', payload)
}
