import Axios from 'axios'
import Localforage from 'localforage'

const http = Axios.create({
  baseURL: process.env.API_BASE,
  timeout: 20000
})

Localforage.getItem('token')
  .then(token => { http.defaults.headers.Authorization = token })

export default {
  getProjects () {
    return new Promise((resolve, reject) => {
      Localforage.getItem('token').then(token => {
        http.defaults.headers.Authorization = token
        http.get('/api/projects').then(response => resolve(response))
          .catch(error => reject(error))
      })
    })
  },
  getProject (id) {
    return new Promise((resolve, reject) => {
      Localforage.getItem('token').then(token => {
        http.defaults.headers.Authorization = token
        http.get(`/api/project/${id}`).then(response => resolve(response))
          .catch(error => reject(error))
      })
    })
  },
  postProject: (payload) => http.post('/api/project', payload),
  putProject: (payload) => http.put(`/api/project/${payload.projectId}`, payload.form),
  getScenarios (projectId) {
    return new Promise((resolve, reject) => {
      Localforage.getItem('token').then(token => {
        http.defaults.headers.Authorization = token
        http.get(`/api/project/${projectId}/scenarios`).then(response => resolve(response))
          .catch(error => reject(error))
      })
    })
  },
  getScenario (params) {
    return new Promise((resolve, reject) => {
      Localforage.getItem('token').then(token => {
        http.defaults.headers.Authorization = token
        http.get(`/api/project/${params.projectId}/scenario/${params.scenarioId}`).then(response => resolve(response))
          .catch(error => reject(error))
      })
    })
  },
  postScenario: (payload) => http.post(`/api/project/${payload.projectId}/scenario`, payload.form),
  putScenario: (payload) => http.put(`/api/project/${payload.projectId}/scenario/${payload.scenarioId}`, payload.form),
  getActors (projectId) {
    return new Promise((resolve, reject) => {
      Localforage.getItem('token').then(token => {
        http.defaults.headers.Authorization = token
        http.get(`/api/project/${projectId}/actors`).then(response => resolve(response))
          .catch(error => reject(error))
      })
    })
  },
  getActor (params) {
    return new Promise((resolve, reject) => {
      Localforage.getItem('token').then(token => {
        http.defaults.headers.Authorization = token
        http.get(`/api/project/${params.projectId}/actor/${params.scenarioId}`).then(response => resolve(response))
          .catch(error => reject(error))
      })
    })
  },
  postActor: (payload) => http.post(`/api/project/${payload.projectId}/actor`, payload.form),
  putActor: (payload) => http.put(`/api/project/${payload.projectId}/actor/${payload.actorId}`, payload.form),
  getStuffs (projectId) {
    return new Promise((resolve, reject) => {
      Localforage.getItem('token').then(token => {
        http.defaults.headers.Authorization = token
        http.get(`/api/project/${projectId}/stuffs`).then(response => resolve(response))
          .catch(error => reject(error))
      })
    })
  },
  getStuff (params) {
    return new Promise((resolve, reject) => {
      Localforage.getItem('token').then(token => {
        http.defaults.headers.Authorization = token
        http.get(`/api/project/${params.projectId}/stuff/${params.scenarioId}`).then(response => resolve(response))
          .catch(error => reject(error))
      })
    })
  },
  postStuff: (payload) => http.post(`/api/project/${payload.projectId}/stuff`, payload.form),
  putStuff: (payload) => http.put(`/api/project/${payload.projectId}/stuff/${payload.stuffId}`, payload.form),
  getPlots (params) {
    return new Promise((resolve, reject) => {
      Localforage.getItem('token').then(token => {
        http.defaults.headers.Authorization = token
        http.get(`/api/project/${params.projectId}/scenario/${params.scenarioId}/plots`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    })
  },
  getPlot (params) {
    return new Promise((resolve, reject) => {
      Localforage.getItem('token').then(token => {
        http.defaults.headers.Authorization = token
        http.get(`/api/project/${params.projectId}/scenario/${params.scenarioId}/plot/${params.plotId}`)
          .then(response => resolve(response))
          .catch(error => reject(error))
      })
    })
  },
  postPlot: (params) => http.post(`/api/project/${params.projectId}/scenario/${params.scenarioId}/plot`, params.form),
  putPlot: (params) => http.put(`/api/project/${params.projectId}/scenario/${params.scenarioId}/plot/${params.plotId}`, params.form),
  getProfile: () => {
    return new Promise((resolve, reject) => {
      Localforage.getItem('token').then(token => {
        http.defaults.headers.Authorization = token
        http.get('/api/account/profile').then(response => resolve(response))
          .catch(error => reject(error))
      })
    })
  }
}
