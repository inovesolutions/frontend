import Localforage from 'localforage'
export default {
  token (state, payload) {
    state.token = payload
    Localforage.setItem('token', payload)
  },
  profile (state, payload) {
    state.me = payload
    Localforage.setItem('me', payload)
  }
}
