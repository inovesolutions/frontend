import PublicRest from '../../rest/public'
import SecureRest from '../../rest/secure'
import Localforage from 'localforage'

export default {
  signIn ({commit}, payload) {
    return new Promise((resolve, reject) => {
      PublicRest.signIn(payload).then(response => {
        commit('token', response.data)
        resolve(response.data)
      }).catch(error => reject(error))
    })
  },
  signUp ({commit}, payload) {
    return new Promise((resolve, reject) => {
      PublicRest.signUp(payload).then(response => {
        commit('token', response.data)
        resolve(response.data)
      }).catch(error => reject(error))
    })
  },
  getProfile ({commit}) {
    return new Promise((resolve, reject) => {
      Localforage.getItem('me').then(me => {
        commit('profile', me)
        SecureRest.getProfile()
          .then(response => {
            commit('profile', response.data)
            resolve(response.data)
          })
          .catch(error => { reject(error) })
      })
    })
  }
}
