import Localforage from 'localforage'
export default {
  projects (state, payload) {
    state.projects = payload
    Localforage.setItem('projects', state.projects)
  },
  project (state, payload) {
    let project = state.projects.find(p => p.id === payload.id)
    if (!project) state.projects.push(payload)
    else {
      let index = state.projects.indexOf(project)
      state.projects.splice(index, 1, payload)
    }
    Localforage.setItem('projects', state.projects)
  }
}
