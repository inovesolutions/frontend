import SecureRest from '../../rest/secure'
import LocalForage from 'localforage'
import Vue from 'vue'

export default {
  getProjects ({commit}) {
    LocalForage.getItem('projects').then(projects => {
      commit('projects', projects || [])
      SecureRest.getProjects().then(response => {
        commit('projects', response.data)
        response.data.forEach(project => {
          Vue.socket.on(project.id, (message) => {
            console.log(message)
          })
        })
      })
    })
  },
  postProject ({commit}, payload) {
    return new Promise((resolve, reject) => {
      SecureRest.postProject(payload).then(response => {
        commit('project', response.data)
        resolve(response.data)
      }).catch(error => reject(error))
    })
  },
  putProject ({commit}, payload) {
    return new Promise((resolve, reject) => {
      SecureRest.putProject(payload).then(response => {
        commit('project', response.data)
        resolve(response.data)
      }).catch(error => reject(error))
    })
  }
}
