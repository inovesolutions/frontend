import auth from './auth'
import project from './project'
import scenario from './scenario'
import actor from './actor'
import stuff from './stuff'
import plot from './plot'
import Es6Promise from 'es6-promise'
Es6Promise.polyfill()

export default {
  strict: false,
  modules: {
    auth,
    project,
    scenario,
    actor,
    stuff,
    plot
  }
}
