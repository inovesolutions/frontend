import Localforage from 'localforage'
import Vue from 'vue'

export default {
  actors (state, payload) {
    Vue.set(state.actors, payload.projectId, payload.data || [])
    Localforage.setItem(`actors.${payload.projectId}`, state.actors[payload.projectId])
  },
  actor (state, payload) {
    if (!state.actors[payload.projectId]) state.actors[payload.projectId] = []
    let actor = state.actors[payload.projectId].find(a => a.id === payload.data.id)
    if (!actor) {
      state.actors[payload.projectId].push(payload.data)
    } else {
      let index = state.actors[payload.projectId].indexOf(actor)
      state.actors[payload.projectId].splice(index, 1, payload.data)
    }
    Localforage.setItem(`actors.${payload.projectId}`, state.actors[payload.projectId])
  }
}
