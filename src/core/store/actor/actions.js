import SecureRest from '../../rest/secure'
import LocalForage from 'localforage'

export default {
  getActors ({commit}, projectId) {
    LocalForage.getItem(`actors.${projectId}`).then(actors => {
      let data = actors || []
      commit('actors', {data, projectId})
      SecureRest.getActors(projectId).then(response => {
        commit('actors', { data: response.data, projectId })
      })
    })
  },
  getActor ({commit}, params) {
    SecureRest.getActor(params).then(response => {
      commit('actor', { data: response.data, projectId: params.projectId })
    })
  },
  postActor ({commit}, payload) {
    return new Promise((resolve, reject) => {
      SecureRest.postActor(payload).then(response => {
        commit('actor', { data: response.data, projectId: payload.projectId })
        resolve(response.data)
      }).catch(error => reject(error))
    })
  },
  putActor ({commit}, payload) {
    return new Promise((resolve, reject) => {
      SecureRest.putActor(payload).then(response => {
        commit('actor', { data: response.data, projectId: payload.projectId })
        resolve(response.data)
      }).catch(error => reject(error))
    })
  }
}
