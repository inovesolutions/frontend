import Localforage from 'localforage'
import Vue from 'vue'

export default {
  stuffs (state, payload) {
    Vue.set(state.stuffs, payload.projectId, payload.data || [])
    Localforage.setItem(`stuffs.${payload.projectId}`, state.stuffs[payload.projectId])
  },
  stuff (state, payload) {
    if (!state.stuffs[payload.projectId]) state.stuffs[payload.projectId] = []
    let stuff = state.stuffs[payload.projectId].find(s => s.id === payload.data.id)
    if (!stuff) {
      state.stuffs[payload.projectId].push(payload.data)
    } else {
      let index = state.stuffs[payload.projectId].indexOf(stuff)
      state.stuffs[payload.projectId].splice(index, 1, payload.data)
    }
    Localforage.setItem(`stuffs.${payload.projectId}`, state.stuffs[payload.projectId])
  }
}
