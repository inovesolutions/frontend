import SecureRest from '../../rest/secure'
import LocalForage from 'localforage'

export default {
  getStuffs ({commit}, projectId) {
    LocalForage.getItem(`stuffs.${projectId}`).then(stuffs => {
      let data = stuffs || []
      commit('stuffs', {data, projectId})
      SecureRest.getStuffs(projectId).then(response => {
        commit('stuffs', { data: response.data, projectId })
      })
    })
  },
  getStuff ({commit}, params) {
    SecureRest.getStuff(params).then(response => {
      commit('stuff', { data: response.data, projectId: params.projectId })
    })
  },
  postStuff ({commit}, payload) {
    return new Promise((resolve, reject) => {
      SecureRest.postStuff(payload).then(response => {
        commit('stuff', { data: response.data, projectId: payload.projectId })
        resolve(response.data)
      }).catch(error => reject(error))
    })
  },
  putStuff ({commit}, payload) {
    return new Promise((resolve, reject) => {
      SecureRest.putStuff(payload).then(response => {
        commit('stuff', { data: response.data, projectId: payload.projectId })
        resolve(response.data)
      }).catch(error => reject(error))
    })
  }
}
