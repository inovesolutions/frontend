import Localforage from 'localforage'
import Vue from 'vue'

export default {
  plots (state, payload) {
    let stateId = `${payload.projectId}.${payload.scenarioId}`
    Vue.set(state.plots, stateId, payload.data || [])
    Localforage.setItem(`plots.${stateId}`, state.plots[stateId])
  },
  plot (state, payload) {
    let stateId = `${payload.projectId}.${payload.scenarioId}`
    if (!state.plots[stateId]) state.plots[stateId] = []
    let plot = state.plots[stateId].find(p => p.id === payload.data.id)
    if (!plot) state.plots[stateId].push(payload.data)
    else {
      let index = state.plots[stateId].indexOf(plot)
      state.plots[stateId].splice(index, 1, payload.data)
    }
    Localforage.setItem(`plots.${stateId}`, state.plots[stateId])
  }
}
