import SecureRest from '../../rest/secure'
import LocalForage from 'localforage'

export default {
  getPlots ({commit}, params) {
    LocalForage.getItem(`plots.${params.projectId}.${params.scenarioId}`).then(plots => {
      let data = plots || []
      commit('plots', {
        data,
        projectId: params.projectId,
        scenarioId: params.scenarioId
      })
      SecureRest.getPlots(params).then(response => {
        commit('plots', {
          data: response.data,
          projectId: params.projectId,
          scenarioId: params.scenarioId
        })
      })
    })
  },
  postPlot ({commit}, params) {
    return new Promise((resolve, reject) => {
      SecureRest.postPlot(params).then(response => {
        commit('plot', {
          data: response.data,
          projectId: params.projectId,
          scenarioId: params.scenarioId
        })
        resolve(response.data)
      }).catch(error => reject(error))
    })
  },
  putPlot ({commit}, params) {
    return new Promise((resolve, reject) => {
      SecureRest.putPlot(params).then(response => {
        commit('plot', {
          data: response.data,
          projectId: params.projectId,
          scenarioId: params.scenarioId
        })
        resolve(response.data)
      }).catch(error => reject(error))
    })
  }
}
