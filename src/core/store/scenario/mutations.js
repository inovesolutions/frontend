import Localforage from 'localforage'
import Vue from 'vue'

export default {
  scenarios (state, payload) {
    Vue.set(state.scenarios, payload.projectId, payload.data || [])
    Localforage.setItem(`scenarios.${payload.projectId}`, state.scenarios[payload.projectId])
  },
  scenario (state, payload) {
    if (!state.scenarios[payload.projectId]) state.scenarios[payload.projectId] = []
    let scenario = state.scenarios[payload.projectId].find(s => s.id === payload.data.id)
    if (!scenario) {
      state.scenarios[payload.projectId].push(payload.data)
    } else {
      let index = state.scenarios[payload.projectId].indexOf(scenario)
      state.scenarios[payload.projectId].splice(index, 1, payload.data)
    }
    Localforage.setItem(`scenarios.${payload.projectId}`, state.scenarios[payload.projectId])
  }
}
