import SecureRest from '../../rest/secure'
import LocalForage from 'localforage'

export default {
  getScenarios ({commit}, projectId) {
    LocalForage.getItem(`scenarios.${projectId}`).then(scenarios => {
      let data = scenarios || []
      commit('scenarios', {data, projectId})
      SecureRest.getScenarios(projectId).then(response => {
        commit('scenarios', { data: response.data, projectId })
      })
    })
  },
  postScenario ({commit}, payload) {
    return new Promise((resolve, reject) => {
      SecureRest.postScenario(payload).then(response => {
        commit('scenario', { data: response.data, projectId: payload.projectId })
        resolve(response.data)
      }).catch(error => reject(error))
    })
  },
  putScenario ({commit}, payload) {
    return new Promise((resolve, reject) => {
      SecureRest.putScenario(payload).then(response => {
        commit('scenario', { data: response.data, projectId: payload.projectId })
        resolve(response.data)
      }).catch(error => reject(error))
    })
  }
}
