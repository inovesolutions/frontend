import Vue from 'vue'
import Router from 'vue-router'
import { routes as auth } from '@/features/auth'
import { routes as main } from '@/features/main'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    ...auth,
    ...main
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.replace('/')

router.beforeEach((to, from, next) => {
  document.title = to.meta.title || 'Scenario'
  next()
})

export default router
