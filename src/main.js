// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'
import Vuetify from 'vuetify'
// import * as colors from 'vuetify/es5/util/colors'
import STORE from './core/store'
import DateFilter from '@/shared/filters/date.filter'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'babel-polyfill'
import * as io from 'socket.io-client'

const Chart = () => import('@/shared/components/chart.component')
const JsonFormatted = () => import('@/shared/components/json-formatted.component')
const App = () => import('@/app.component')

Vue.config.productionTip = false
Vue.prototype.$eventBus = new Vue()

Vue.use(Vuex)
const store = new Vuex.Store(STORE)
Vue.store = store
Vue.router = router

Vue.use(Vuetify, {
  theme: {
    primary: '#505F79',
    info: '#3882A0',
    error: '#BE7DA0',
    secondary: '#8291AD',
    warning: '#DBA11C',
    success: '#00AAA0',
    accent: '#29C7CD'
  }
})

let socket = io.connect('http://localhost:3000')
Vue.socket = socket

Vue.component('chart', Chart)
Vue.component('json-formatted', JsonFormatted)
Vue.filter('date', DateFilter)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
