const AuthPage = () => import('@/features/auth/pages/auth.page')
const SignInPage = () => import('@/features/auth/pages/signin.page')
const SignUpPage = () => import('@/features/auth/pages/signup.page')
export default [
  {
    path: '/',
    name: 'AuthPage',
    component: AuthPage,
    children: [
      {
        path: 'signin',
        name: 'SignInPage',
        meta: {
          title: 'Acessar conta'
        },
        component: SignInPage
      },
      {
        path: 'signup',
        name: 'SignUpPage',
        meta: {
          title: 'Criar conta'
        },
        component: SignUpPage
      }
    ]
  }
]
