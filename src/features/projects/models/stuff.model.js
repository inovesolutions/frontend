export default class Stuff {
  constructor () {
    this.name = null
    this.description = null
    this.creationDate = null
    this.model = { properties: {} }
    this.collection = null
  }
}
