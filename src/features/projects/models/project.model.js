export default class Project {
  constructor () {
    this.name = null
    this.description = null
    this.creationDate = null
    this.domains = ['http://localhost']
  }
}
