const ProjectsPage = () => import('@/features/projects/pages/project-list.page')
const ToolbarComponent = () => import('@/features/projects/components/toolbar.component')
const ProjectPage = () => import('@/features/projects/pages/project.page')

export default {
  list: [{
    path: 'projects',
    name: 'main.ProjectsPage',
    components: {
      default: ProjectsPage,
      toolbarItems: ToolbarComponent
    },
    meta: {
      title: 'Projetos'
    }
  }],
  detail: [{
    path: '/project/:id',
    name: 'main.ProjectPage',
    components: {
      default: ProjectPage
    },
    meta: {
      title: 'Detalhes do projeto'
    }
  }]
}
