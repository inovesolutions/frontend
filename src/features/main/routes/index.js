import {routes as projects} from '../../projects'
import {routes as scenario} from '../../scenario'
import {routes as settings} from '../../settings'
const MainPage = () => import('@/features/main/pages/main.page')
const DashboardPage = () => import('@/features/main/pages/dashboard.page')

export default [{
  path: '/',
  name: 'MainPage',
  component: MainPage,
  children: [
    {
      path: 'dashboard',
      component: DashboardPage,
      name: 'main.DashboardPage',
      meta: {
        title: 'Painel de controle'
      }
    },
    ...projects.list,
    ...settings
  ]
},
...projects.detail,
...scenario
]
