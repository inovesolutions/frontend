export default class Query {
  constructor () {
    this.tableName = null
    this.command = null
    this.statement = {}
    this.orderField = null
    this.fields = []
    this.matchField = null
  }
}
