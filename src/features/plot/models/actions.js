const actions = {
  VALIDATION: 'validation',
  OPERATION: 'operation',
  QUERY: 'query',
  INTEGRATION: 'integration'
}

export default actions
