import Validation from './validation.model'
import Query from './query.model'
import Operation from './operation.model'

export default class Stage {
  constructor () {
    this.id = null
    this.action = null
    this.name = null
    this.optional = true
    this.validation = new Validation()
    this.query = new Query()
    this.operation = new Operation()
    this.webhook = null
    this.actor = null
    this.stuff = null
  }
}
