import PlotPage from '../pages/plot.page'
export default [
  {
    path: '/project/:projectId/scenario/:scenarioId/plot/:plotId',
    name: 'main.PlotPage',
    component: PlotPage,
    meta: {
      title: 'Detalhes da cena'
    }
  }
]
