const SettingsPage = () => import('@/features/settings/pages/settings.page')
export default [{
  path: 'settings',
  component: SettingsPage,
  name: 'main.SettingsPage',
  components: {
    default: SettingsPage
  },
  meta: {
    title: 'Configurações'
  }
}]
