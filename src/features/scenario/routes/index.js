import ScenarioPage from '../pages/scenario.page'
export default [
  {
    path: '/project/:projectId/scenario/:scenarioId',
    name: 'main.ScenarioPage',
    component: ScenarioPage,
    meta: {
      title: 'Detalhes do cenário'
    }
  }
]
