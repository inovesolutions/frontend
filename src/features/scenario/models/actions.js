const actions = {
  VALIDATION: 'validation',
  INSERT: 'insert',
  UPDATE: 'update',
  QUERY: 'query',
  INTEGRATION: 'integration'
}

export default actions
