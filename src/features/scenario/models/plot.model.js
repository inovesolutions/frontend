export default class Plot {
  constructor () {
    this.id = null
    this.name = null
    this.description = null
    this.creationDate = null
    this.scenarioId = null
    this.action = null
    this.stuff = null
    this.actor = null
    this.payload = {}
  }
}
