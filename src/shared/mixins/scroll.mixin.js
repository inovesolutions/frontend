export default {
  data () {
    return {
      scrolling: false,
      scrollingTimeout: null,
      scrollingDirection: 'bottom',
      lastScrollTop: 0
    }
  },
  methods: {
    onScroll ($event) {
      if (document.scrollingElement.scrollTop > 0) {
        this.scrolling = true
        if (this.scrollingTimeout) clearTimeout(this.scrollingTimeout)
      }

      let offset = window.pageYOffset || document.documentElement.scrollTop
      if (offset > this.lastScrollTop) {
        this.scrollingDirection = 'bottom'
      } else {
        this.scrollingDirection = 'top'
      }

      this.lastScrollTop = offset <= 0 ? 0 : offset

      setTimeout(() => {
        this.scrolling = false
        this.scrollingDirection = 'bottom'
      }, 1000)
    }
  }
}
