import moment from 'moment'

export default (value, pattern) => {
  if (value) {
    return moment(String(value)).format(pattern)
  }
}
